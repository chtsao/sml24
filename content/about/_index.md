+++
title = 'About'
date = 2024-02-15T20:16:02+08:00
draft = false
category = ["admin"]
tags = ["textbook"]
+++

<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpaperaccess.com%2Ffull%2F1426962.jpg&f=1&nofb=1&ipt=5919a927e4dfc331ee6e1e34f7d5daa1dfeb81fb1dd3e05982de2f69e6038c33&ipo=images" alt="Robots" style="zoom:50%;" />


### 課程資訊

* Curator: C. Andy Tsao.  Office: SE A411.  Tel: 3520, [Syllabus](https://sys.ndhu.edu.tw/AA/CLASS/TeacherSubj/prt_tplan.aspx?no=112254031)
* Lectures: 
  * 3D: Mon. **1310**-1400, Thr. **1410**-1500 @ AE B301.
  * Virtual: GoogleClassRoom, Discord 
* Office Hours:  Mon. 14:10-15:00, Thr. 15:10-16:00. @ SE A411 or by appointment. (Mentor Hour:  Tue/Thr 1200-1300. 也歡迎來，但若有導生來，則導生優先)
* Computing: R ([original](https://cran.r-project.org/), , [mirrors](https://cran.r-project.org/mirrors.html)); [Rstudio](https://www.rstudio.com/)  (IDE for R). You are welcome to use other languages or softwares nonetheless, e.g. Julia, Python, Matlab.
* Textbook: [Hastie, Tibshirani and Friedman (2009). The Elements of Statistical Learning: Data Mining, Inference and Prediction.](https://hastie.su.domains/ElemStatLearn/) 2nd Edition. (aka. ESLII)  Springer-Verlag. Or  [Legal download from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=844743&location=0&q=Elements+of+Statlstical+Machine+Learning&start=0&view=CONTENT), [SpringerLink Legal download from NDHU domain](https://link.springer.com/book/10.1007/978-0-387-84858-7)
* Prerequisites: Statistics, Linear Algebra. Knowledge about
regression or General/Generalized Linear Models will be helpful. 
