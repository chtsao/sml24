+++
title = 'Week 14. Project SML 2024'
date = 2024-05-20T08:33:33+08:00
draft = false
tags= ["project"]

+++

![](https://www.insidescience.org/sites/default/files/2020-07/Vassily_Kandinsky%2C_1923_-_Composition_8%2C_huile_sur_toile%2C_140_cm_x_201_cm%2C_Mus%C3%A9e_Guggenheim%2C_New_York.jpg)



# 公告
停課通知： 5/27 (Mon) Kno 臨時有事，停課一次。抱歉！

## Overview

#### [Data Science Project Overview @kbc](https://chtsao.gitlab.io/pmod/blogs/dataproject/)
#### [ML pipeline@IBM](https://www.ibm.com/topics/machine-learning-pipeline)

## Admin

#### Recap: 

##### Each team will 

* analyze a data set of your choice
* using methods we have discussed in this class or combining with  regression/classification methods you have learned earlier (or consult with me if you have other  preferred methods )   

##### Choosing a data set
* personal or academic interest to you
*  fits one of the analyses studied in SML  
*  a real data set which you have not analyzed before and which has not been analyzed in a textbook  is preferred (or consult with me if you prefer using a  data set of this kind)

#### Project Part I– Project Proposal/Roadmap discussion  [Scrum on May 23 (Thr), May 27 (Mon)]

Each team should write a one to two-page typed description of the data set you propose to study.  You should include details about the response variable, about (potential) predictor variables, and about the number of observations.  If there are issues such as obvious small n large p problem etc., comment on these and suggest possible remedies.   Discuss the source of the data set, and whether the data come from an experimental or observational study.

In addition, please include a half-page printout of the data set (or if it is quite large, a selected part of the data set).  

You should also include a general proposal for what sort of analysis you plan to do with this data set.  If there are any hypotheses/research questions that are of interest from the beginning, you might mention those.

#### Project Part II – Presentation [Late May and Early June]
* Time: Tentatively, May 30 (Thr), June 3 (Mon), ...
* The presentation should be an oral presentation of your Written Report (detailed in Project Part III).  
* The presentation time is 45 min for each team. Questions and comments will be raised/given in the presentation. 

#### Project Part III – Written Report [Due June 16, 23:59 to my univ. email]

You should write a concise report summarizing your analysis.  The report should be no longer than six (typed) pages, not counting any R output, graphs, etc., which you may wish to include as support or illustration for your analysis. 

The style of the report is up to you, but the best reports will address many of the questions and details studied in class when we discussed the relevant type of analysis.

Some things to include (depending on the data set and choice of model) might be: 

- An introduction and discussion of the data set itself
- Brief intro to the questions we would like to address and related literature
- Summary of your results (answers to the questions and implications)
- Discussion of the classifiers under your study: performance and possible explanation for the performance. Comparison of linear and nonlinear classifiers/ model-based classifiers/distance-based classifiers.
- Your overall conclusions about the data, based on your analysis

Note: The *project information* in http://www.stat.sc.edu/~hitchcock/stat704.html 
is used as a template for this guideline. 