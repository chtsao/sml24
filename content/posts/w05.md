+++
title = 'Week 5. Optimal (Bayes) Machine/Learning Method'
date = 2024-03-18T07:07:00+08:00
draft = false
categories= ["notes"]
tags = ["posterior probability", "classification problem", "posterior mean", "regression problem", "Bayes Theorem"]
+++

![BayesTheorem](https://bayesian.org/wp-content/uploads/2016/08/cropped-Bayes_Theorem_MMB_02.jpg)

### Course Data Project Team: Objectives and ~~Evaluation~~ Assessment
Team up and proceed accordingly. Progress check 3/25.
* [Data Project Overview](https://chtsao.gitlab.io/pmod/blogs/dataproject/) at [kbc](https://chtsao.gitlab.io/pmod/categories/kbc/)
* [How to structure datascience team: key models and roles](https://www.altexsoft.com/blog/how-to-structure-data-science-team-key-models-and-roles/)
* [Glasswing Venture](https://glasswing.vc/): [AI Paletes](https://glasswing.vc/ai-palette/)
* [Super Data Science Podcast](https://www.superdatascience.com/podcast): SDS 763, SDS 765.
* [星箭廣播](https://www.listennotes.com/podcasts/%E6%98%9F%E7%AE%AD%E5%BB%A3%E6%92%AD-star-rocket-cSZ4rQWudhl/) 

### Bayes Theorem
* $P(A|B) =  \frac{P(B|A)P(A)}{P(B|A)P(A) + P(B|A^c)P(A^c)}$
* When $X, Y$ are discrete r.v. with pmf's 
 $$ P(Y=y|X=x) = \frac{P(X=x|Y=y)P(Y=y)}{\sum_{u \in \mathcal{Y}} P(X=x|Y=u)P(Y=u)} $$
* Let $f_{X, Y}, f_{X}, f_{Y}$ be the joint pdf, marginal pdf of $X$, marginal pmf of discrete $Y$ respectively,
 $$ f_{Y|X}(y|x) = \frac{f_{X|Y}(x|y)f_{Y}(y)}{\sum_{u \in \mathcal{Y}}f_{X|Y}(x|u)f_{Y}(u)}$$ 

### Formulation
Given \\( Y, X \sim P_{Y, X}\\) where \\( Y \in \mathcal{Y} \subset \mathcal{R}, X \in \mathcal{R}^p\\). The (theoretical/formal) machine learning problem is to find a good function 
$$ F^*: \mathcal{R}^p \rightarrow \mathcal{Y}$$ 
and minimizes the EPE (expected predicted error or Bayes risk)
$$ EPE(F)=E_{Y, X}L(Y, F(X)) = E_X E_{Y|X}L(Y, F(X))$$
for all \\( F \in \mathcal{F},\\) some class of functions/machines. 
#### Regression problem
For regression problem, \\( \mathcal{Y} = \mathcal{R}\\). For square error loss function \\( L(y, F(x)) = (y-F(x))^2\\)  and given \\( X=x\\), the pointwise minimizer for \\( E_{Y|x}L(Y, F(x))\\) can be found. Specifically, 
$$ F_\pi (x) = arg \min_{F \in \mathcal{F}} E_{Y|x}L(Y, F(x))= E(Y|x).$$
Note: KNN and LSE/Stat-Regression \\( \approx \\) \\( E(Y|x).\\)
#### Classification problem
For classification problem, \\( \mathcal{Y} = \\) {1, 2, ..., K }. For 0-1 loss function \\( L(y, G(x)) = 1_{[y \neq G(x)]}\\)  and given \\( X=x\\), the pointwise minimizer for \\( E_{Y|x}L(Y, G(x))\\) can be found. Specifically, 
$$ G_\pi (x) = arg \min_{G \in \mathcal{F}}  E_{Y|x}L(Y, G(x)) = arg \max_{y \in \mathcal{Y}} P(Y=y|x).$$
Note: KNN/Logistic Regression, FDA \\( \approx arg \max_{y \in \mathcal{Y}} P(Y=y|x).\\) 

##### Fisher Discriminant Analysis

* LDA
* QDA

### ~~Wake-up~~ Warm-up Exercise
1. Follow the instruction/description of Section 2.3.3 in the textbook, simulate the data accordingly and construct  (your team) Figure 2.4.  How does your figure compare with Figure 2.4 given in the textbook? What do these figures suggest? What's the point the authors want to make and do you agree with them?

