+++
title = 'Week 8. Annoucement'
date = 2024-04-09T18:45:11+08:00
draft = false
categories=["adm"]
+++
![](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEiSKCZIZcizqmGR8OsIAI8wc67ngn6_b_pXhwh-VmaTGAre17dHvaotTA75wNX4GhrFzIRmTPc9yZ33u5idKNIEEMq4gm3Z0jFsTs32zjBG58InN1HMjAM0-6oA8qNPhiPD6dhPVOg7AvM/s1600/Earthrise1_Apollo8AndersWeigang_2048.jpg)

各位同學平安！ 配合[學校應變措施]([NDHU AA Announcement](https://announce.ndhu.edu.tw/mail_display.php?timestamp=1712476487) )，4/10-4/26 採線上上課。後續的事項會在此課網以及 SML24@Discord 公佈。

* 邀請函請 check 學校 gmail
* 另以小組為單位寄 組名，組員名單 給我。 

這門課程接下來會以Course Project 為軸，來學習理解 data project 的進行，以及各種不同的機器學習方法。也已建制了 Discord 平台方便後續的沙盤、測試以及討論。讓我們一起玩得開心！ 