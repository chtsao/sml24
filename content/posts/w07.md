+++
title = 'Week 7. Project and Data'
date = 2024-04-01T11:00:31+08:00
draft = false
categories= ["notes", "adm"]
+++
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.wFp1JW-NLd6p7O4CUC7yjAHaFP%26pid%3DApi&f=1&ipt=b544f15f9b48fc0f4f7aecfc30e3b78f23c53b23f4da42719c623f02037a4fda&ipo=images"  />

## Project

* SML Data project
* (optional) SML method intro video
* [Data Project Overview @K書重心](https://chtsao.gitlab.io/pmod/blogs/dataproject/)

Make it your ~~Home~~ Work. 

1. Motivating Question
2. Focus problem $\leadsto$ prelim Formulation
3. Data 
4. Prelim EDA
5. Prelim Models
6. Prelim Remarks on Focus problem and feasibility

Cycling 2--5. 

### Data Science Projects

* [Top 6 Data Science Projects for Beginners to Get Hired in 2022](https://www.simplilearn.com/data-science-projects-article)
* [Top 10 Data Science Projects in 2022](https://hackr.io/blog/data-science-projects)
* [12 Data Science Projects To Try (From Beginner to Advanced)](https://www.springboard.com/blog/data-science/data-science-projects/)
* Papers and TechReports: [Arxiv](https://arxiv.org/), ResearchGate
* [全國碩博士論文](https://ndltd.ncl.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=Y4Sv56/login?jstimes=1&loadingjs=1&o=dwebmge&ssoauth=1&cache=1711940788840)

  
### Data

* [data.org](https://data.org/)
* Kaggle alternatives: [alternative to](https://alternativeto.net/), [similar sites](https://www.alexa.com/find-similar-sites)


### Project Management

* Github/Gitlab
* Scrum
  * [Scrum Guide 2022](https://scrumguides.org/scrum-guide.html) @ scrumguide.org 
  * [**什麼是Scrum？**](https://medium.com/doflowy/%E4%BB%80%E9%BA%BC%E6%98%AFscrum-%E4%B8%8D%E6%98%AF%E5%B7%A5%E7%A8%8B%E5%B8%AB%E4%B9%9F%E8%83%BD%E6%87%82%E7%9A%84scrum%E5%85%A5%E9%96%80%E6%95%99%E5%AD%B8-1cc6683575f8)
  * [eduScrum](https://eduscrum.org/)
* [From Syntax Errors to Smooth Code: Debugging Tips in R Studio from Expert Tutors ](https://ourcodeworld.com/articles/read/1268/from-syntax-errors-to-smooth-code-debugging-tips-in-r-studio-from-expert-tutors)